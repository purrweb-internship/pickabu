import { InputType, PartialType } from '@nestjs/graphql';
import { SignupUserInput } from 'src/auth/inputs/signup-user.input';

@InputType()
export class UpdateUserInput extends PartialType(SignupUserInput) {}
