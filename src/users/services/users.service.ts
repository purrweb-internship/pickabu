import { Injectable } from '@nestjs/common';
import { UpdateUserInput } from '../inputs/update-user.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { SignupUserInput } from 'src/auth/inputs/signup-user.input';
import { HashService } from './hash.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private repo: Repository<User>,
    private hashService: HashService,
  ) {}

  async create(input: SignupUserInput): Promise<User> {
    input.password = await this.hashService.generateHash(input.password);
    return await this.repo.save(input);
  }

  async findOne(id: string): Promise<User> {
    return this.repo.findOneOrFail(id);
  }

  async update(id: string, input: UpdateUserInput): Promise<User> {
    if (input.password) {
      input.password = await this.hashService.generateHash(input.password);
    }
    await this.repo.update(id, input);
    return this.findOne(id);
  }

  async remove(id: string): Promise<boolean> {
    const user: User = await this.findOne(id);
    await this.repo.remove(user);
    return true;
  }
}
