import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { UserModel } from '../../models/user.model';
import { UsersService } from '../../services/users.service';
import { UpdateUserInput } from '../../inputs/update-user.input';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';

@UseGuards(JwtAuthGuard)
@Resolver(() => UserModel)
export class UsersMutationResolver {
  constructor(private readonly usersService: UsersService) {}

  @Mutation(() => UserModel)
  async updateUser(
    @Self('id') id: string,
    @Args('updateUserInput') input: UpdateUserInput,
  ): Promise<UserModel> {
    const user = await this.usersService.update(id, input);
    return UserModel.create(user);
  }

  @Mutation(() => Boolean)
  async removeUser(@Self('id') id: string): Promise<boolean> {
    return this.usersService.remove(id);
  }
}
