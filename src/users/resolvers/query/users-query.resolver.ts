import { Query, Resolver } from '@nestjs/graphql';
import { UserModel } from '../../models/user.model';
import { UsersService } from '../../services/users.service';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';

@UseGuards(JwtAuthGuard)
@Resolver(() => UserModel)
export class UsersQueryResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query(() => UserModel, { name: 'user' })
  async findOne(@Self('id') id: string): Promise<UserModel> {
    const user = await this.usersService.findOne(id);
    return UserModel.create(user);
  }
}
