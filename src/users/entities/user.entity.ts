import { Column, Entity, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { BaseEntity } from 'src/base/entities/base.entity';
import { Post } from 'src/posts/entities/post.entity';
import { Comment } from 'src/comments/entities/comment.entity';
import { Activity } from '../../activities/entities/activity.entity';

@Entity('users')
export class User extends BaseEntity {
  @Column('varchar', { unique: true })
  email!: string;

  @Column('varchar')
  @Exclude({ toPlainOnly: true })
  password!: string;

  @OneToMany(() => Post, (post) => post.user)
  posts!: Post[];

  @OneToMany(() => Comment, (comment) => comment.user)
  comments!: Comment[];

  @OneToMany(() => Activity, (activity) => activity.user)
  activities!: Activity[];
}
