import { NestDataLoader } from '@purrweb/dataloader';
import { UserModel } from '../models/user.model';
import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserLoader implements NestDataLoader<string, UserModel> {
  constructor(
    @InjectRepository(User) private readonly repo: Repository<User>,
  ) {}

  generateDataLoader(): DataLoader<string, UserModel> {
    return new DataLoader<string, UserModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const users = await this.repo.findByIds(uniqueKeys as string[]);

      return keys.map(
        (key) =>
          users.find((user) => user.id === key) ||
          new Error(`Could not load user with id: ${key}`),
      );
    });
  }
}
