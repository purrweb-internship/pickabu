import { define } from 'typeorm-seeding';
import { Faker } from '@faker-js/faker';
import { Comment } from 'src/comments/entities/comment.entity';

define(Comment, (faker: Faker) => {
  const comment = new Comment();

  comment.text = faker.lorem.text();
  comment.image = faker.internet.url();

  return comment;
});
