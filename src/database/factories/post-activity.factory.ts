import { define } from 'typeorm-seeding';
import { Faker } from '@faker-js/faker';
import { PostActivity } from 'src/activities/entities/post-activity.entity';

define(PostActivity, (faker: Faker) => {
  const postActivity = new PostActivity();

  postActivity.isFavorite = faker.datatype.boolean();
  postActivity.isLike = faker.datatype.boolean();

  return postActivity;
});
