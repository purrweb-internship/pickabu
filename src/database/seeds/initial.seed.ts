import { Factory, Seeder } from 'typeorm-seeding';
import { CommentActivity } from 'src/activities/entities/comment-activity.entity';
import { PostActivity } from 'src/activities/entities/post-activity.entity';
import { User } from '../../users/entities/user.entity';
import { Post } from '../../posts/entities/post.entity';
import { Comment } from '../../comments/entities/comment.entity';

export class InitialSeed implements Seeder {
  public async run(factory: Factory): Promise<void> {
    const users = await factory(User)().createMany(15);

    const posts = await Promise.all(
      users.map((user) =>
        factory(Post)().create({
          user: user,
        }),
      ),
    );

    const comments = await Promise.all(
      posts.map((post) =>
        factory(Comment)().create({
          user: post.user,
          post: post,
        }),
      ),
    );

    posts.map((post) =>
      factory(PostActivity)().create({
        post: post,
        user: post.user,
      }),
    );

    comments.map((comment) =>
      factory(CommentActivity)().create({
        user: comment.user,
        comment: comment,
      }),
    );
  }
}
