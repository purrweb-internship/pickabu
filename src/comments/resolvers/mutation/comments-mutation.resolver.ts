import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { CommentsService } from '../../comments.service';
import { CreateCommentInput } from '../../inputs/create-comment.input';
import { UpdateCommentInput } from '../../inputs/update-comment.input';
import { CommentModel } from '../../models/comment.model';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';
import { IsOwnerCommentGuard } from '../../guards/is-owner-comment.guard';

@UseGuards(JwtAuthGuard)
@Resolver(() => CommentModel)
export class CommentsMutationResolver {
  constructor(private readonly commentsService: CommentsService) {}

  @Mutation(() => CommentModel)
  async createComment(
    @Self('id') userId: string,
    @Args('createCommentInput') input: CreateCommentInput,
  ): Promise<CommentModel> {
    const comment = await this.commentsService.create(userId, input);
    return CommentModel.create(comment);
  }

  @UseGuards(IsOwnerCommentGuard)
  @Mutation(() => CommentModel)
  async updateComment(
    @Args('id', ParseUUIDPipe) id: string,
    @Args('updateCommentInput') input: UpdateCommentInput,
  ): Promise<CommentModel> {
    const comment = await this.commentsService.update(id, input);
    return CommentModel.create(comment);
  }

  @UseGuards(IsOwnerCommentGuard)
  @Mutation(() => Boolean)
  removeComment(@Args('id') id: string): Promise<boolean> {
    return this.commentsService.remove(id);
  }
}
