import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CommentModel } from '../models/comment.model';
import { UserModel } from 'src/users/models/user.model';
import { Loader } from '@purrweb/dataloader';
import { UserLoader } from 'src/users/dataloaders/user.loader';
import DataLoader from 'dataloader';

@Resolver(() => CommentModel)
export class CommentsResolver {
  @ResolveField('author', () => UserModel)
  author(
    @Parent() comment: CommentModel,
    @Loader(UserLoader) userLoader: DataLoader<string, UserModel>,
  ): Promise<UserModel> {
    const { userId } = comment;
    return userLoader.load(userId);
  }
}
