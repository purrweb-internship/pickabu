import { NestDataLoader } from '@purrweb/dataloader';
import { CommentModel } from '../models/comment.model';
import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from '../entities/comment.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CommentLoader implements NestDataLoader<string, CommentModel> {
  constructor(
    @InjectRepository(Comment)
    private readonly repo: Repository<Comment>,
  ) {}

  generateDataLoader(): DataLoader<string, CommentModel> {
    return new DataLoader<string, CommentModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const comments = await this.repo.findByIds(uniqueKeys as string[]);

      return keys.map(
        (key) =>
          comments.find((comment) => comment.id === key) ||
          new Error(`Could not load comment with id: ${key}`),
      );
    });
  }
}
