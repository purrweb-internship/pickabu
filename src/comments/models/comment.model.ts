import { BaseModel } from 'src/base/models/base.model';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Comment } from '../entities/comment.entity';
import { plainToInstance } from 'class-transformer';

@ObjectType('Comment')
export class CommentModel extends BaseModel {
  @Field()
  text!: string;

  @Field({ nullable: true })
  image?: string;

  @Field(() => ID)
  userId!: string;

  @Field(() => ID)
  postId!: string;

  static create(entity: Comment): CommentModel {
    return plainToInstance(this, entity);
  }
}
