import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Post } from 'src/posts/entities/post.entity';
import { BaseEntity } from 'src/base/entities/base.entity';
import { CommentActivity } from 'src/activities/entities/comment-activity.entity';

@Entity('comments')
export class Comment extends BaseEntity {
  @Column('varchar')
  text!: string;

  @Column('varchar', { nullable: true })
  image?: string;

  @Column('uuid')
  userId!: string;

  @ManyToOne(() => User, (user) => user.comments, { onDelete: 'CASCADE' })
  user!: User;

  @Column('uuid')
  postId!: string;

  @ManyToOne(() => Post, (post) => post.comments, { onDelete: 'CASCADE' })
  post!: Post;

  @OneToMany(() => CommentActivity, (activity) => activity.comment)
  activities!: CommentActivity[];
}
