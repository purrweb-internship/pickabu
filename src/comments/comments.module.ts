import { Module } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { CommentsMutationResolver } from './resolvers/mutation/comments-mutation.resolver';
import { CommentsResolver } from './resolvers/comments.resolver';
import { UserLoader } from 'src/users/dataloaders/user.loader';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Comment, User])],
  providers: [
    CommentsResolver,
    CommentsMutationResolver,
    CommentsService,
    UserLoader,
  ],
  exports: [CommentsService],
})
export class CommentsModule {}
