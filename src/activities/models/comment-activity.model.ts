import { RatingModel } from './rating.model';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { FavoriteModel } from './favorite.model';
import { plainToInstance } from 'class-transformer';
import { CommentActivity } from '../entities/comment-activity.entity';

@ObjectType('FavoriteComment')
export class FavoriteCommentModel extends FavoriteModel {
  @Field(() => ID)
  commentId!: string;

  static create(entity: CommentActivity) {
    return plainToInstance(this, entity);
  }
}

@ObjectType('RatingComment')
export class RatingCommentModel extends RatingModel {
  @Field(() => ID)
  commentId!: string;

  static create(entity: CommentActivity) {
    return plainToInstance(this, entity);
  }
}
