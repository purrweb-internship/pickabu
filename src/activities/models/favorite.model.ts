import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ isAbstract: true })
export class FavoriteModel {
  @Field()
  isFavorite!: boolean;

  @Field(() => ID)
  userId!: string;
}
