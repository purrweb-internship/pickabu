import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ isAbstract: true })
export class RatingModel {
  @Field({ nullable: true })
  isLike?: boolean;

  @Field(() => ID)
  userId!: string;
}
