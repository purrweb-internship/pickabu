import { FavoriteActivityService } from './favorite-activity.service';
import { Injectable } from '@nestjs/common';
import { CommentActivityRepository } from '../repositories/comment-activity.repository';
import { CommentActivity } from '../entities/comment-activity.entity';
import { RatingActivityService } from './rating-activity.service';

@Injectable()
export class FavoriteCommentsService extends FavoriteActivityService<CommentActivity> {
  constructor(repository: CommentActivityRepository) {
    super(repository);
  }
}

@Injectable()
export class RatingCommentsService extends RatingActivityService<CommentActivity> {
  constructor(repository: CommentActivityRepository) {
    super(repository);
  }
}
