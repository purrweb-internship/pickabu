import { Injectable } from '@nestjs/common';
import { Activity } from '../entities/activity.entity';
import { ActivityRepository } from '../repositories/activity.repository';
import { DeepPartial, FindConditions, IsNull, Not } from 'typeorm';

@Injectable()
export class RatingActivityService<T extends Activity> {
  protected constructor(private repo: ActivityRepository<T>) {}

  async setRate(dto: DeepPartial<T>): Promise<T> {
    const { isLike, ...conditions } = dto;
    return this.repo.updateOrInsert(dto, {
      where: { ...conditions },
    });
  }

  async removeRate(conditions: FindConditions<T>): Promise<boolean> {
    const rating = await this.repo.findOneOrFail(conditions, {
      where: { isLike: Not(IsNull()) },
    });
    await this.repo.save({ ...rating, isLike: null });

    if (!rating.isFavorite) {
      await this.repo.remove(rating);
    }
    return true;
  }
}
