import { Injectable } from '@nestjs/common';
import { PostActivity } from '../entities/post-activity.entity';
import { PostActivityRepository } from '../repositories/post-activity.repository';
import { RatingActivityService } from './rating-activity.service';
import { FavoriteActivityService } from './favorite-activity.service';

@Injectable()
export class RatingPostsService extends RatingActivityService<PostActivity> {
  constructor(repository: PostActivityRepository) {
    super(repository);
  }
}

@Injectable()
export class FavoritePostsService extends FavoriteActivityService<PostActivity> {
  constructor(repository: PostActivityRepository) {
    super(repository);
  }
}
