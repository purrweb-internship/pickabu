import { Activity } from './activity.entity';
import { ChildEntity, Column, ManyToOne, Unique } from 'typeorm';
import { Comment } from 'src/comments/entities/comment.entity';

@ChildEntity()
@Unique(['userId', 'commentId'])
export class CommentActivity extends Activity {
  @Column('uuid')
  commentId!: string;

  @ManyToOne(() => Comment, (comment) => comment.activities)
  comment!: Comment;
}
