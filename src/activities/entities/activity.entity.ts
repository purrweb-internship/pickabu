import { Column, Entity, ManyToOne, TableInheritance } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { BaseEntity } from 'src/base/entities/base.entity';

@Entity('activities')
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
export abstract class Activity extends BaseEntity {
  @Column('boolean', { default: false })
  isFavorite!: boolean;

  @Column('boolean', { nullable: true })
  isLike?: boolean;

  @Column('uuid')
  userId!: string;

  @ManyToOne(() => User, (user) => user.activities)
  user!: User;
}
