import { ChildEntity, Column, ManyToOne, Unique } from 'typeorm';
import { Activity } from './activity.entity';
import { Post } from 'src/posts/entities/post.entity';

@ChildEntity()
@Unique(['userId', 'postId'])
export class PostActivity extends Activity {
  @Column('uuid')
  postId!: string;

  @ManyToOne(() => Post, (post) => post.activities)
  post!: Post;
}
