import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { FavoritePostModel } from '../models/post-activity.model';
import { PostModel } from 'src/posts/models/post.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from 'src/posts/entities/post.entity';
import { Repository } from 'typeorm';
import { Loader } from '@purrweb/dataloader';
import DataLoader from 'dataloader';
import { PostLoader } from 'src/posts/dataloaders/post.loader';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Resolver(() => FavoritePostModel)
export class FavoritePostsResolver {
  constructor(
    @InjectRepository(Post)
    private readonly postsRepository: Repository<Post>,
  ) {}

  @ResolveField('post', () => PostModel)
  post(
    @Parent() favorite: FavoritePostModel,
    @Loader(PostLoader) postLoader: DataLoader<string, PostModel>,
  ): Promise<PostModel> {
    const { postId } = favorite;
    return postLoader.load(postId);
  }
}
