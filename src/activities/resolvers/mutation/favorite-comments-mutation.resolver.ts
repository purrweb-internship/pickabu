import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { FavoriteCommentModel } from '../../models/comment-activity.model';
import { FavoriteCommentsService } from '../../services/comments-activity.service';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';
import { CommentActivity } from '../../entities/comment-activity.entity';

@UseGuards(JwtAuthGuard)
@Resolver(() => FavoriteCommentModel)
export class FavoriteCommentsMutationResolver {
  constructor(private favoriteCommentsService: FavoriteCommentsService) {}

  @Mutation(() => FavoriteCommentModel)
  async addCommentToFavorite(
    @Self('id') userId: string,
    @Args('commentId', ParseUUIDPipe) commentId: string,
  ): Promise<FavoriteCommentModel> {
    const entity: CommentActivity =
      await this.favoriteCommentsService.addToFavorite({
        userId,
        commentId,
      });
    return FavoriteCommentModel.create(entity);
  }

  @Mutation(() => Boolean)
  removeCommentFromFavorite(
    @Self('id') userId: string,
    @Args('commentId', ParseUUIDPipe) commentId: string,
  ): Promise<boolean> {
    return this.favoriteCommentsService.removeFromFavorite({
      userId,
      commentId,
    });
  }
}
