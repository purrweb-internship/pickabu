import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { RatingPostModel } from '../../models/post-activity.model';
import { RatingPostsService } from '../../services/posts-activity.service';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { Self } from 'src/base/decorators/self.decorator';
import { SetRateInput } from '../../inputs/set-rate.input';
import { PostActivity } from '../../entities/post-activity.entity';

@UseGuards(JwtAuthGuard)
@Resolver(() => RatingPostModel)
export class RatingPostsMutationResolver {
  constructor(private ratingPostsService: RatingPostsService) {}

  @Mutation(() => RatingPostModel)
  async setRateToPost(
    @Self('id') userId: string,
    @Args('postId', ParseUUIDPipe) postId: string,
    @Args('setRateInput') input: SetRateInput,
  ): Promise<RatingPostModel> {
    const entity: PostActivity = await this.ratingPostsService.setRate({
      userId,
      postId,
      ...input,
    });
    return RatingPostModel.create(entity);
  }

  @Mutation(() => Boolean)
  removePostRate(
    @Self('id') userId: string,
    @Args('postId', ParseUUIDPipe) postId: string,
  ): Promise<boolean> {
    return this.ratingPostsService.removeRate({
      userId,
      postId,
    });
  }
}
