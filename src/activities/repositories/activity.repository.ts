import {
  DeepPartial,
  EntityRepository,
  FindOneOptions,
  Repository,
} from 'typeorm';
import { Activity } from '../entities/activity.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
@EntityRepository(Activity)
export abstract class ActivityRepository<
  T extends Activity,
> extends Repository<T> {
  async updateOrInsert(
    dto: DeepPartial<T>,
    options?: FindOneOptions<T>,
  ): Promise<T> {
    const existEntity: T | undefined = await this.findOne(options);
    return this.save({ id: existEntity?.id, ...dto });
  }
}
