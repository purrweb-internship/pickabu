import { ObjectLiteral } from 'nestjs-typeorm-paginate/dist/interfaces';
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class PaginationMeta implements ObjectLiteral {
  @Field(() => Int)
  itemCount!: number;
  /**
   * the total amount of items
   */
  @Field(() => Int)
  totalItems?: number;
  /**
   * the amount of items that were requested per page
   */
  @Field(() => Int)
  itemsPerPage!: number;
  /**
   * the total amount of pages in this paginator
   */
  @Field(() => Int)
  totalPages?: number;
  /**
   * the current page this paginator "points" to
   */
  @Field(() => Int)
  currentPage!: number;
}
