import { ArgsType, Field } from '@nestjs/graphql';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export enum Group {
  HOT = 'HOT',
  BEST = 'BEST',
  RECENT = 'RECENT',
}

@ArgsType()
export class FilterArgs {
  @Field(() => [String], { nullable: true })
  @IsOptional()
  @IsString({ each: true })
  tags?: string[];

  @Field({ nullable: true })
  @IsOptional()
  @IsBoolean()
  forLastDay!: boolean;
}
