import { NestDataLoader } from '@purrweb/dataloader';
import { PostModel } from '../models/post.model';
import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from '../entities/post.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PostLoader implements NestDataLoader<string, PostModel> {
  constructor(
    @InjectRepository(Post)
    private readonly repo: Repository<Post>,
  ) {}

  generateDataLoader(): DataLoader<string, PostModel> {
    return new DataLoader<string, PostModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const posts = await this.repo.findByIds(uniqueKeys as string[]);
      return keys.map(
        (key) =>
          posts.find((post) => post.id === key) ||
          new Error(`Could not load post with id: ${key}`),
      );
    });
  }
}
