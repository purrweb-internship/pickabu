import { Module } from '@nestjs/common';
import { PostsService } from './services/posts.service';
import { PostsResolver } from './resolvers/posts.resolver';
import { PostsMutationResolver } from './resolvers/mutation/posts-mutation.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from './entities/post.entity';
import { CommentsModule } from 'src/comments/comments.module';
import { PostsQueryResolver } from './resolvers/query/posts-query.resolver';
import { UserLoader } from 'src/users/dataloaders/user.loader';
import { User } from 'src/users/entities/user.entity';
import { PostQueryBuilderFactory } from './services/post-query-builder.factory';
import { PostsLikesViewEntity } from './views/posts-likes-view.entity';
import { PostsCommentsViewEntity } from './views/posts-comments-view.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Post,
      User,
      PostsLikesViewEntity,
      PostsCommentsViewEntity,
    ]),
    CommentsModule,
  ],
  providers: [
    PostsResolver,
    PostsMutationResolver,
    PostsQueryResolver,
    PostsService,
    UserLoader,
    PostQueryBuilderFactory,
  ],
  exports: [PostsService],
})
export class PostsModule {}
