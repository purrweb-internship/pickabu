import { Injectable } from '@nestjs/common';
import { CreatePostInput } from '../inputs/create-post.input';
import { UpdatePostInput } from '../inputs/update-post.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from '../entities/post.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { SortArgs } from 'src/base/args/sort.args';
import { FilterArgs } from 'src/base/args/filter.args';
import { SearchArgs } from 'src/base/args/search.args';
import { PostQueryBuilderFactory } from './post-query-builder.factory';
import { PostsLikesViewEntity } from '../views/posts-likes-view.entity';
import { PostsCommentsViewEntity } from '../views/posts-comments-view.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post) private repo: Repository<Post>,
    private qbFactory: PostQueryBuilderFactory,
  ) {}

  async create(userId: string, input: CreatePostInput): Promise<Post> {
    return this.repo.save({ userId, ...input });
  }

  async findAll(
    options: IPaginationOptions,
    { sort, order }: SortArgs,
    { searchValue }: SearchArgs,
    { tags, forLastDay }: FilterArgs,
  ): Promise<
    Pagination<Post | PostsCommentsViewEntity | PostsLikesViewEntity>
  > {
    const qb: SelectQueryBuilder<
      Post | PostsCommentsViewEntity | PostsLikesViewEntity
    > = this.qbFactory.create(sort);

    if (forLastDay) {
      qb.andWhere("created_at >= Now() - INTERVAL '24 hours'");
    }

    if (tags) {
      qb.andWhere(':...tags = ANY(tags)', { tags: tags });
    }

    if (searchValue) {
      qb.andWhere('title LIKE :searchValue', {
        searchValue: `%${searchValue}%`,
      });
    }
    qb.orderBy(sort, order);

    return paginate<Post | PostsLikesViewEntity | PostsCommentsViewEntity>(
      qb,
      options,
    );
  }

  async findOne(id: string): Promise<Post> {
    return this.repo.findOneOrFail(id);
  }

  async update(id: string, input: UpdatePostInput): Promise<Post> {
    await this.repo.update(id, input);
    return this.findOne(id);
  }

  async remove(id: string): Promise<boolean> {
    const post: Post = await this.findOne(id);
    await this.repo.remove(post);
    return true;
  }
}
