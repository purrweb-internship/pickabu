import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from 'src/base/models/base.model';
import { Post } from '../entities/post.entity';
import { plainToInstance } from 'class-transformer';
import { PostsLikesViewEntity } from '../views/posts-likes-view.entity';
import { PostsCommentsViewEntity } from '../views/posts-comments-view.entity';

@ObjectType('Post')
export class PostModel extends BaseModel {
  @Field()
  title!: string;

  @Field({ nullable: true })
  text?: string;

  @Field(() => [String], { defaultValue: [] })
  images!: string[];

  @Field(() => [String], { defaultValue: [] })
  tags!: string[];

  @Field(() => ID)
  userId!: string;

  static create(
    entity: Post | PostsLikesViewEntity | PostsCommentsViewEntity,
  ): PostModel {
    return plainToInstance(this, entity);
  }
}
