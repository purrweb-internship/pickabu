import { Field, ObjectType } from '@nestjs/graphql';
import { PostModel } from './post.model';
import { PaginatedModel } from 'src/base/models/paginated.model';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Post } from '../entities/post.entity';
import { plainToInstance } from 'class-transformer';
import { PostsLikesViewEntity } from '../views/posts-likes-view.entity';
import { PostsCommentsViewEntity } from '../views/posts-comments-view.entity';

@ObjectType('PaginatedPosts')
export class PaginatedPostsModel extends PaginatedModel {
  @Field(() => [PostModel])
  items!: PostModel[];

  static create(
    pagination: Pagination<
      Post | PostsLikesViewEntity | PostsCommentsViewEntity
    >,
  ) {
    const items = pagination.items.map(PostModel.create);
    return plainToInstance(this, { items: items, meta: pagination.meta });
  }
}
