import { ViewColumn } from 'typeorm';
import { Post } from '../entities/post.entity';

export class PostView {
  @ViewColumn()
  id!: Post['id'];

  @ViewColumn()
  userId!: Post['userId'];

  @ViewColumn()
  title!: Post['title'];

  @ViewColumn()
  text?: Post['text'];

  @ViewColumn()
  images!: Post['images'];

  @ViewColumn()
  tags!: Post['tags'];
}
