import { Args, Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { PostModel } from '../models/post.model';
import { PaginationArgs } from 'src/base/args/pagination.args';
import { PaginatedCommentsModel } from 'src/comments/models/paginated-comments.model';
import { CommentsService } from 'src/comments/comments.service';
import { UserModel } from 'src/users/models/user.model';
import { SortArgs } from 'src/base/args/sort.args';
import { Loader } from '@purrweb/dataloader';
import { UserLoader } from 'src/users/dataloaders/user.loader';
import DataLoader from 'dataloader';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Comment } from '../../comments/entities/comment.entity';

@Resolver(() => PostModel)
export class PostsResolver {
  constructor(private readonly commentsService: CommentsService) {}

  @ResolveField('author', () => UserModel)
  async author(
    @Parent() post: PostModel,
    @Loader(UserLoader) userLoader: DataLoader<string, UserModel>,
  ): Promise<UserModel> {
    const { userId } = post;
    return userLoader.load(userId);
  }

  @ResolveField('comments', () => PaginatedCommentsModel)
  async comments(
    @Args() paginationArgs: PaginationArgs,
    @Args() sortArgs: SortArgs,
    @Parent() post: PostModel,
  ): Promise<PaginatedCommentsModel> {
    const postId = post.id;
    const entities: Pagination<Comment> = await this.commentsService.paginate(
      paginationArgs,
      sortArgs,
      postId,
    );
    return PaginatedCommentsModel.create(entities);
  }
}
