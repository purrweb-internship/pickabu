import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { PostsService } from '../../services/posts.service';
import { CreatePostInput } from '../../inputs/create-post.input';
import { UpdatePostInput } from '../../inputs/update-post.input';
import { PostModel } from '../../models/post.model';
import { Self } from 'src/base/decorators/self.decorator';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/strategies/jwt/jwt-auth.guard';
import { IsOwnerPostGuard } from '../../guards/is-owner-post.guard';

@UseGuards(JwtAuthGuard)
@Resolver(() => PostModel)
export class PostsMutationResolver {
  constructor(private readonly postsService: PostsService) {}

  @Mutation(() => PostModel)
  async createPost(
    @Self('id') userId: string,
    @Args('createPostInput') input: CreatePostInput,
  ): Promise<PostModel> {
    const post = await this.postsService.create(userId, input);
    return PostModel.create(post);
  }

  @UseGuards(IsOwnerPostGuard)
  @Mutation(() => PostModel)
  async updatePost(
    @Args('id', ParseUUIDPipe) id: string,
    @Args('updatePostInput') input: UpdatePostInput,
  ): Promise<PostModel> {
    const post = await this.postsService.update(id, input);
    return PostModel.create(post);
  }

  @UseGuards(IsOwnerPostGuard)
  @Mutation(() => Boolean)
  removePost(@Args('id') id: string): Promise<boolean> {
    return this.postsService.remove(id);
  }
}
