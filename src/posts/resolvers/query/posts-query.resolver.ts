import { Args, Query, Resolver } from '@nestjs/graphql';
import { PostModel } from '../../models/post.model';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { PaginatedPostsModel } from '../../models/paginated-posts.model';
import { PaginationArgs } from 'src/base/args/pagination.args';
import { SortArgs } from 'src/base/args/sort.args';
import { SearchArgs } from 'src/base/args/search.args';
import { FilterArgs } from 'src/base/args/filter.args';
import { PostsService } from '../../services/posts.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Post } from '../../entities/post.entity';
import { PostsLikesViewEntity } from '../../views/posts-likes-view.entity';
import { PostsCommentsViewEntity } from '../../views/posts-comments-view.entity';

@Resolver(() => PostModel)
export class PostsQueryResolver {
  constructor(
    private readonly postsService: PostsService,
    @InjectEntityManager() private readonly entityManager: EntityManager,
  ) {}

  @Query(() => PaginatedPostsModel)
  async postsFind(
    @Args() paginationArgs: PaginationArgs,
    @Args() sortArgs: SortArgs,
    @Args() searchArgs: SearchArgs,
    @Args() filterArgs: FilterArgs,
  ): Promise<PaginatedPostsModel> {
    const entities: Pagination<
      Post | PostsLikesViewEntity | PostsCommentsViewEntity
    > = await this.postsService.findAll(
      paginationArgs,
      sortArgs,
      searchArgs,
      filterArgs,
    );
    return PaginatedPostsModel.create(entities);
  }

  @Query(() => PostModel, { name: 'post' })
  async postFind(@Args('id') id: string): Promise<PostModel> {
    const post = await this.postsService.findOne(id);
    return PostModel.create(post);
  }
}
