import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { BaseEntity } from 'src/base/entities/base.entity';
import { Comment } from 'src/comments/entities/comment.entity';
import { PostActivity } from 'src/activities/entities/post-activity.entity';

@Entity('posts')
export class Post extends BaseEntity {
  @Column('varchar')
  title!: string;

  @Column('varchar', { nullable: true })
  text?: string;

  @Column('varchar', { array: true, default: [] })
  images!: string[];

  @Column('varchar', { array: true, default: [] })
  tags!: string[];

  @Column('uuid')
  userId!: string;

  @ManyToOne(() => User, (user) => user.posts, { onDelete: 'CASCADE' })
  user!: User;

  @OneToMany(() => Comment, (comment) => comment.post)
  comments!: Comment[];

  @OneToMany(() => PostActivity, (activity) => activity.post)
  activities!: PostActivity[];
}
