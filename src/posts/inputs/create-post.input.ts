import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsString } from 'class-validator';

@InputType()
export class CreatePostInput {
  @Field()
  @IsString()
  title!: string;

  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  text?: string;

  @Field(() => [String], { defaultValue: [] })
  @IsString({ each: true })
  @IsOptional()
  images?: string[];

  @Field(() => [String], { defaultValue: [] })
  @IsOptional()
  @IsString({ each: true })
  tags?: string[];
}
